(local CONF {:_AUTHOR "parlortricks"
             :_EMAIL "palortricks@fastmail.fm"
             :_NAME "SNE"
             :_VERSION "1.0"
             :_URL "https://gitlab.com/build-engine/sne"
             :_DESCRIPTION "Shaw's Nightmare Extracter"
             :_SPDX "MIT License"
             :_SPDXURL "https://spdx.org/licenses/MIT"
             :_LICENSE "    MIT LICENSE
    Copyright 2022 parlortricks <parlortricks@fastmail.fm>

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the \"Software\"), to 
    deal in the Software without restriction, including without limitation the 
    rights to use, copy, modify, merge, publish, distribute, sublicense, and/or 
    sell copies of the Software, and to permit persons to whom the Software is 
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in 
    all copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED \"AS IS\", WITHOUT WARRANTY OF ANY KIND, EXPRESS 
    OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE 
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING 
    FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
    IN THE SOFTWARE."})

(set CONF._HELP (..
"=====================================================\n"
CONF._DESCRIPTION " v" CONF._VERSION "\n" 
"=====================================================\n"
CONF._SPDX " -> " CONF._SPDXURL "\n"
"Copyright 2022 " CONF._AUTHOR " <" CONF._EMAIL ">\n\n"
CONF._URL "\n"
"=====================================================\n\n"
"Usage: fennel sne.fnl [archive] 
  
  Options:
  -h, --help            Display this help
  -l, --list [archive]  Display contents of archive
  -v, --version         Display the version"))

(local archive-header "DAT\026")
(local archive {:files []})
(var list-files false)

(fn exists? [file]
  (let [(ok err code) (os.rename file file)]
    (when (not ok)
      (when (= code 13)
        (lua "return true")))
    (values ok err)))

(fn get-header [fin]
  (set archive.numitems (string.unpack "<i2" (fin:read 2)))
  (set archive.index-offset (string.unpack "<i4" (fin:read 4)))
  (set archive.index-size (string.unpack "<i4" (fin:read 4))))

(fn get-meta-data [fin]
  (fin:seek :set archive.index-offset)
  (for [i 1 archive.numitems]
    (let [file {}]
      (set file.name (: (string.unpack "c16" (fin:read 16)) :gsub "%.?\0+$" ""))
      (set file.size (string.unpack "i4" (fin:read 4)))
      (set file.offset (string.unpack "i4" (fin:read 4)))
      (table.insert archive.files file))))

(fn get-file-data [fin]
  (each [_ file (ipairs archive.files)]
    (fin:seek :set file.offset)
    (set file.data (fin:read file.size))))

(fn save [data]
  (each [i file (ipairs data)]
      (with-open [fout (io.open (.. "extract/" file.name) :wb)]
        (fout:write file.data))))

(fn save-files-to-disk [data]
  (if (not (exists? "extract/"))
    (do
      (local os (require :os))
      (os.execute (.. :mkdir " " :extract))))
  (each [i file (ipairs data)]
    (with-open [fout (io.open (.. "extract/" file.name) :wb)]
      (fout:write file.data))))

(fn process-archive [file list-files]
  (if (exists? file)
    (with-open [fin (io.open file :rb)]
      (let [file-header (string.unpack "c4" (fin:read 4))]
        (if (= archive-header file-header)
          (do
            (set archive.header file-header)
            (get-header fin)
            (get-meta-data fin)
            (get-file-data fin)
            (if list-files
              (each [_ file (ipairs archive.files)]
                (print file.name))
              (save-files-to-disk archive.files)))
          (do ;; Unknown archive type
            (print "Unknown archive")))))
    (do
      (print "Archive doesnt exist."))))   

(if (or (= (length arg) 0) (: (. arg 1) :match "-h") (: (. arg 1) :match "--help"))
      (print CONF._HELP)
    (or (: (. arg 1) :match "-v") (: (. arg 1) :match "--version"))
      (print (.. CONF._NAME " v" CONF._VERSION))
    (or (: (. arg 1) :match "-L") (: (. arg 1) :match "--list"))
      (do 
        (set list-files true)
        (process-archive (. arg 2) list-files))
    (process-archive (. arg 1) list-files))

